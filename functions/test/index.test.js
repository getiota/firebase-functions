var assert = require('assert')
var rewire = require('rewire')

var app = rewire('../index.js')

const test = require('firebase-functions-test')()

describe('Subscribe', () => {
  describe('#validateEmail', () => {
    validateEmail = app.__get__('validateEmail')

    const invalidEmails = ['hello', 'not-an-email', 'hello@hello@']
    invalidEmails.forEach((email) => {
      it(`Returns false for ${email}`, () => {
        assert.ok(!validateEmail(email))
      })
    })

    const validEmails = ['hello@projet-iota.eu']
    validEmails.forEach((email) => {
      it(`Returns true for ${email}`, () => {
        assert.ok(validateEmail(email))
      })
    })
  })

  describe('#verifyRequest', () => {
    verifyRequest = app.__get__('verifyRequest')
    it('Returns the email', () => {
      req = { body: { email: 'hello@hello.com' } }

      const { email } = verifyRequest(req)
      assert.equal(email, 'hello@hello.com')
    })

    const errorBodies = [{}, { email: 'not-an-email' }]

    errorBodies.forEach((content) => {
      it('Raises for an invalid body', () => {
        assert.throws(() => {
          verifyRequest({ body: content })
        })
      })
    })
  })

  describe('#findIPAddress', () => {
    findIPAddress = app.__get__('findIPAddress')
    it('Returns the ip from the request', () => {
      assert.equal(findIPAddress({ ip: 'hello' }), 'hello')
    })

    it('Returns the IP from the fastly header', () => {
      assert.equal(
        findIPAddress({ headers: { 'fastly-client-ip': 'hello1' } }),
        'hello1'
      )
    })

    it('Returns the IP from the forwarded header', () => {
      assert.equal(
        findIPAddress({ headers: { 'x-forwarded-for': 'hello2' } }),
        'hello2'
      )
    })
  })
})
