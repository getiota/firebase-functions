var assert = require('assert')
var rewire = require('rewire')

var ratelimit = rewire('../ratelimit.js')

describe('Subscribe', () => {
  describe('#updateLimit', () => {
    const updateLimit = ratelimit.__get__('updateLimit')

    const limiter1 = { count: 1, delay: 10, additive: 'true' }

    it('1: Returns the correct empty value', () => {
      var [success, value] = updateLimit(limiter1, 0, null)
      assert.ok(success)
      assert.deepStrictEqual(value, { count: 0, deadline: 10 })
    })

    it('1: Returns a failure if the request is repeated', () => {
      var [success, value] = updateLimit(limiter1, 0, {
        count: 0,
        deadline: 10,
      })
      assert.ok(!success)
      assert.deepStrictEqual(value, { count: 1, deadline: 20 })
    })

    const limiter2 = { count: 2, delay: 100, additive: false }

    it('2: Accepts counts below its limit', () => {
      var [success, value] = updateLimit(limiter2, 0, {
        count: 0,
        deadline: 100,
      })
      assert.ok(success)
      assert.deepStrictEqual(value, { count: 1, deadline: 100 })
    })

    it('2: Denies after the limit is reached', () => {
      var [success, value] = updateLimit(limiter2, 0, {
        count: 1,
        deadline: 100,
      })
      assert.ok(!success)
      assert.deepStrictEqual(value, { count: 2, deadline: 100 })
    })

    it('2: Resets once the deadline is passed', () => {
      var [success, value] = updateLimit(limiter2, 101, {
        count: 2,
        deadline: 100,
      })
      assert.ok(success)
      assert.deepStrictEqual(value, { count: 0, deadline: 201 })
    })
  })

  describe('#verifyLimits', () => {
    const verifyLimits = ratelimit.__get__('verifyLimits')
    const limiters = [
      { count: 1, delay: 10, additive: 'true', slug: '1' },
      { count: 2, delay: 20, additive: 'false', slug: '2' },
    ]

    it('Sets all empty values', () => {
      const [success, value] = verifyLimits(limiters, 0, {})
      assert.ok(success)
      assert.deepStrictEqual(value, {
        '1': {
          count: 0,
          deadline: 10,
        },
        '2': {
          count: 0,
          deadline: 20,
        },
      })
    })

    it('Skips subsequent updates if one fails', () => {
      const [success, value] = verifyLimits(limiters, 0, {
        '1': { count: 0, deadline: 10 },
      })

      assert.ok(!success)
      assert.deepStrictEqual(value, {
        '1': { count: 1, deadline: 20 },
      })
    })

    it('Updates all values', () => {
      const [success, value] = verifyLimits(limiters, 11, {
        '1': { count: 0, deadline: 10 },
        '2': { count: 0, deadline: 20 },
      })

      assert.ok(success)
      assert.deepStrictEqual(value, {
        '1': { count: 0, deadline: 21 },
        '2': { count: 1, deadline: 20 },
      })
    })
  })
})
