const assert = require('assert')
const rewire = require('rewire')

var recaptcha = rewire('../recaptcha.js')

describe('ReCaptcha', () => {
  describe('#validate', () => {
    it('Resolves a success', (done) => {
      recaptcha.__set__('nanor.request', (options, data) => {
        console.log('hello')
        assert.equal(data, 'secret=recaptchasecret&response=RESPONSE')
        assert.equal(options.headers['Content-Length'], 40)
        return Promise.resolve({ success: true })
      })

      recaptcha
        .validate('recaptchasecret', 'RESPONSE')
        .then(done)
        .catch((error) => {
          done(new Error(`Validating catcha failed with ${error}`))
        })
    })

    it('Rejects an error', (done) => {
      recaptcha.__set__('nanor.request', (options, data) => {
        assert.equal(data, 'secret=recaptchasecret&response=RESPONSE')
        assert.equal(options.headers['Content-Length'], 40)
        return Promise.resolve({ success: false })
      })

      recaptcha
        .validate('RESPONSE')
        .then(() => {
          assert.fail()
          done()
        })
        .catch(() => {
          done()
        })
    })
  })
})
