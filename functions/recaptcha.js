const nanor = require('./nanor')
const querystring = require('querystring')

/**
 * Validates the captcha
 * @param {string} captcha
 */
async function validate(secret, captcha) {
  if (!captcha) {
    throw 403
  }

  const payload = querystring.stringify({
    secret: secret,
    response: captcha,
  })

  //TODO: add user IP?
  const options = {
    hostname: 'www.google.com',
    port: 443,
    path: '/recaptcha/api/siteverify',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': payload.length,
    },
  }

  body = await nanor.request(options, payload)
  if (!body.success) {
    throw 403
  }
}

module.exports = {
  validate: validate,
}
