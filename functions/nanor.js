const https = require('https')
const agent = new https.Agent({ keepAlive: true })

/**
 * Simple wrapper around https that adds Promises
 * @param {object} options
 * @param {string} data
 */
async function request(options, data) {
  options.agent = agent

  return new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {
      res.setEncoding('utf8')
      let responseBody = ''

      res.on('data', (chunk) => {
        responseBody += chunk
      })

      res.on('end', () => {
        try {
          resolve(JSON.parse(responseBody))
        } catch (e) {
          reject(e)
        }
      })
    })

    req.on('error', (err) => {
      reject(err)
    })

    req.write(data)
    req.end()
  })
}

module.exports = {
  request: request,
}
