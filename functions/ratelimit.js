const functions = require('firebase-functions')

function updateLimit(limiter, now, value) {
  const delay = parseInt(limiter.delay)

  // The limit was not created so we set an empty limit
  if (!value || now > value.deadline) {
    return [true, { count: 0, deadline: now + delay }]
  }

  const additive = limiter.additive === 'true'

  count = value.count + 1
  deadline = value.deadline
  success = true
  if (count >= limiter.count) {
    // Firestore stores confs as strings
    if (additive) {
      // Adding an exponential delay
      deadline += count * count * delay
    }

    success = false
  }
  return [success, { count: count, deadline: deadline }]
}

function verifyLimits(limiters, now, limits) {
  const fullSuccess = limiters.every((element) => {
    var [success, limit] = updateLimit(element, now, limits[element.slug])
    limits[element.slug] = limit
    return success
  })
  return [fullSuccess, limits]
}

async function verify(database, qualifier) {
  const collection = functions.config().ratelimit.collection
  const limiters = functions.config().ratelimit.limiters

  ref = database.ref(`${collection}/${qualifier}`)

  value = (await ref.once('value')).val() || {}
  const now = Math.floor(new Date().getTime() / 1000)

  const [success, newValue] = verifyLimits(limiters, now, value)

  // Performing transaction asyncrhonously
  // There is no need to wait for it to return
  ref
    .transaction(() => {
      return newValue
    })
    .catch(() => {
      console.error(`Could not write value ${newValue}`)
    })

  if (!success) {
    throw 429 // Too many requests
  }
}

module.exports = {
  verify: verify,
}
