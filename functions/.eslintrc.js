module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 2017,
  },
  extends: ['prettier', 'plugin:prettier/recommended'],
  plugins: ['prettier', 'mocha'],
  rules: {
    'no-console': 'off',
  },
}
