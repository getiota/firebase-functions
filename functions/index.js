const functions = require('firebase-functions')
const admin = require('firebase-admin')
const nanor = require('./nanor')
const ratelimit = require('./ratelimit')

const domains = functions.config().cors ? functions.config().cors.domains : null

const cors = require('cors')({
  origin: domains ? new RegExp(domains) : true,
})

admin.initializeApp(functions.config().firebase)

/**
 * Validates that a string is an email
 */
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

/**
 * Verifies that the request contains the required parameters
 * and returns extracted values
 *
 * @param {request} request
 * @returns {{email: string}}
 * @throws 'Invalid'
 */
function verifyRequest(request) {
  const email = request.body.email

  if (!email || !validateEmail(email)) {
    throw 400
  }
  return {
    email: email,
  }
}

/**
 * Creates a contact in MailJet
 * @param {string} email
 */
async function registerEmail(email) {
  const payload = JSON.stringify({
    Email: email,
  })

  const mailjetConf = functions.config().mailjet

  const auth = Buffer.from(
    mailjetConf.username + ':' + mailjetConf.password
  ).toString('base64')

  const options = {
    hostname: 'api.mailjet.com',
    port: 443,
    path: '/v3/REST/contact',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': payload.length,
      Authorization: 'Basic ' + auth,
    },
  }

  await nanor.request(options, payload)
}

function findIPAddress(request) {
  return (
    request.ip ||
    request.headers['fastly-client-ip'] ||
    request.headers['x-forwarded-for']
  )
}

async function _subscribe(request) {
  const database = admin.database()

  const ip = findIPAddress(request)
  await ratelimit.verify(database, ip)

  const { email } = verifyRequest(request)

  // await recaptcha.validate(
  //   functions.config().recaptcha.secret,
  //   request.body.captcha
  // )

  await registerEmail(email)
}

exports.subscribe = functions
  .region('europe-west1')
  .https.onRequest((request, response) => {
    if (!['POST', 'OPTIONS'].includes(request.method)) {
      return response.status(405).send()
    }

    return cors(request, response, () => {
      response.status(200).send()
      // _subscribe(request)
      //   .then(() => {
      //     response.status(201).send()
      //   })
      //   .catch((e) => {
      //     if (Number.isInteger(e)) {
      //       response.status(e).send()
      //     } else {
      //       console.error(e)
      //       response.status(500).send()
      //     }
      //   })
    })
  })
