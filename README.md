# Firebase Functions for the Projet IOTA Landing

This repository contains a set of functions to be used from the project
iota landing page.

## Required function config

```json
{
  "recaptcha": {
    "secret": ""
  },
  "ratelimit": {
    "limiters": [
      {
        "count": "10",
        "delay": "1000",
        "additive": "true",
        "slug": "limiter1"
      }
    ],
    "collection": "ratelimit"
  },
  "mailjet": {
    "password": "",
    "username": ""
  }
}
```